var selectElementsStartingWithA = function(array) {
    return array.filter(e => e.charAt(0) === 'a');
}

var selectElementsStartingWithVowel = function(array) {
    const vowels = ['a', 'e', 'y', 'o', 'i', 'u'];
    return array.filter(e => vowels.includes(e.charAt(0)));
}

var removeNullElements = function(array) {
    return array.filter(e => e != null);
}

var removeNullAndFalseElements = function(array) {
    const falses = [null, false];
    return array.filter(e => !falses.includes(e));
}

var reverseWordsInArray = function(array) {
    let reverseWord = (word) => {
        let output = "";
        for (let i = word.length - 1; i >= 0; i--) {
            output += word[i];
        }
        return output;
    }
    return array.map(e => reverseWord(e));
}

var everyPossiblePair = function(array) {
    return "write your methode here!";
}

var allElementsExceptFirstThree = function(array) {
    return array.splice(3);
}

var addElementToBeginning = function(array, element) {
    const output = array;
    array.unshift(element)
    return output;
}

var sortByLastLetter = function(array) {
    return array.sort((a, b) => a.charCodeAt(a.length - 1) - b.charCodeAt(b.length - 1));
}

var getFirstHalf = function(string) {
    return string.slice(0, Math.round(string.length / 2));
}

var makeNegative = function(number) {
    return -Math.abs(number);
}

var numberOfPalindromes = function(array) {
    const checkPalindrome = (word) => {
        return word == word.split('').reverse().join('');
    }
    const palindromes = array.filter(e => checkPalindrome(e));
    return palindromes.length;
}

var shortestWord = function(array) {
    return array.reduce((a, b) => a.length <= b.length ? a : b);
}

var longestWord = function(array) {
    return array.reduce((a, b) => a.length >= b.length ? a : b);
}

var sumNumbers = function(array) {
    return array.reduce((acc, e) => acc + e);
}

var repeatElements = function(array) {
    const duplicate = [...array];
    array.push(...duplicate);
    return array;
}

var stringToNumber = function(string) {
    return parseInt(string);
}

var calculateAverage = function(array) {
    let sum = array.reduce((acc, e) => acc + e);
    const average = (sum / array.length) || 0;
    return average;
}

var getElementsUntilGreaterThanFive = function(array) {
    let output = [];
    let i = 0;
    do {
        output.push(array[i]);
        i++;
    } while (array[i] < 6);
    return output;
}

var convertArrayToObject = function(array) {
    const obj = {};
    array.forEach((element, i) => {
        if (i % 2 === 0) {
            obj[element] = array[i + 1];
        }
    });
    return obj;
}

var getAllLetters = function(array) {
    const string = array.join('');
    const unique_letters = String.prototype.concat(...new Set(string));
    return Array.from(unique_letters).sort((a, b) => a.localeCompare(b));
}

var swapKeysAndValues = function(object) {
    const output = {};
    for (const property in object) {
        const old_key = property;
        const old_value = object[property];
            output[old_value] = old_key;
    }
    return output;
}

var sumKeysAndValues = function(object) {
    const output = [];
    for (const property in object) {
        output.push(parseInt(property));
        output.push(parseInt(object[property]));
    }
    return output.reduce((acc, e) => acc + e);
}

var removeCapitals = function(string) {
    return string.replace(/[A-Z]/g, '');
}

var roundUp = function(number) {
    return Math.ceil(number);
}

var formatDateNicely = function(date) {
    return 'Write your method here';
}

var getDomainName = function(string) {
    return 'Write your method here';
}

var titleize = function(string) {
    return 'Write your method here';
}

var checkForSpecialCharacters = function(string) {
    return 'Write your method here';
}

var squareRoot = function(number) {
    return 'Write your method here';
}

var factorial = function(number) {
    return 'Write your method here';
}

var findAnagrams = function(string) {
    return 'Write your method here';
}

var convertToCelsius = function(number) {
    return 'Write your method here';
}

var letterPosition = function(array) {
    return 'Write your method here';
}
